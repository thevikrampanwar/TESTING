package com.example.websocket.bean;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderExecutionReport{
    private String memberId;
    private String currency;
    private String symbol;
    private BigDecimal originalQtyBeforeScale;
    private BigDecimal  qty;
    private BigDecimal price;
    private BigDecimal executedQty;// total executed qty
    private BigDecimal executedPrice ;
    private BigDecimal cancelledQty ;
    private BigDecimal leavesQty ;
    private OrderSide side;
    private OrderType type;
    private MarketExchangeEnum marketExchange;
    private long transactTime;
    private String childOrderId; // our order id, generated by
    private String orderId; //
    private String exchangeOrderId; // the venue gives to us
    private OrderStatus orderStatus;
    private String exchangeAccountId; // keep track of accounts on their venue
    private boolean childOrder; // indicates wether it is a child or not

    // for different symbols in different exchanges, the precision may be different
    private int pricePrecision;
    private int qtyPrecision; // quantity precision
    private OrderReportType reportType;

    private String rejectReason;
    private int rejectCode;

    private BigDecimal lastFilledQty; // total executed qty
    private BigDecimal lastFilledPrice;
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public BigDecimal getOriginalQtyBeforeScale() {
		return originalQtyBeforeScale;
	}
	public void setOriginalQtyBeforeScale(BigDecimal originalQtyBeforeScale) {
		this.originalQtyBeforeScale = originalQtyBeforeScale;
	}
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getExecutedQty() {
		return executedQty;
	}
	public void setExecutedQty(BigDecimal executedQty) {
		this.executedQty = executedQty;
	}
	public BigDecimal getExecutedPrice() {
		return executedPrice;
	}
	public void setExecutedPrice(BigDecimal executedPrice) {
		this.executedPrice = executedPrice;
	}
	public BigDecimal getCancelledQty() {
		return cancelledQty;
	}
	public void setCancelledQty(BigDecimal cancelledQty) {
		this.cancelledQty = cancelledQty;
	}
	public BigDecimal getLeavesQty() {
		return leavesQty;
	}
	public void setLeavesQty(BigDecimal leavesQty) {
		this.leavesQty = leavesQty;
	}
	public OrderSide getSide() {
		return side;
	}
	public void setSide(OrderSide side) {
		this.side = side;
	}
	public OrderType getType() {
		return type;
	}
	public void setType(OrderType type) {
		this.type = type;
	}
	public MarketExchangeEnum getMarketExchange() {
		return marketExchange;
	}
	public void setMarketExchange(MarketExchangeEnum marketExchange) {
		this.marketExchange = marketExchange;
	}
	public long getTransactTime() {
		return transactTime;
	}
	public void setTransactTime(long transactTime) {
		this.transactTime = transactTime;
	}
	public String getChildOrderId() {
		return childOrderId;
	}
	public void setChildOrderId(String childOrderId) {
		this.childOrderId = childOrderId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getExchangeOrderId() {
		return exchangeOrderId;
	}
	public void setExchangeOrderId(String exchangeOrderId) {
		this.exchangeOrderId = exchangeOrderId;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getExchangeAccountId() {
		return exchangeAccountId;
	}
	public void setExchangeAccountId(String exchangeAccountId) {
		this.exchangeAccountId = exchangeAccountId;
	}
	public boolean isChildOrder() {
		return childOrder;
	}
	public void setChildOrder(boolean childOrder) {
		this.childOrder = childOrder;
	}
	public int getPricePrecision() {
		return pricePrecision;
	}
	public void setPricePrecision(int pricePrecision) {
		this.pricePrecision = pricePrecision;
	}
	public int getQtyPrecision() {
		return qtyPrecision;
	}
	public void setQtyPrecision(int qtyPrecision) {
		this.qtyPrecision = qtyPrecision;
	}
	public OrderReportType getReportType() {
		return reportType;
	}
	public void setReportType(OrderReportType reportType) {
		this.reportType = reportType;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public int getRejectCode() {
		return rejectCode;
	}
	public void setRejectCode(int rejectCode) {
		this.rejectCode = rejectCode;
	}
	public BigDecimal getLastFilledQty() {
		return lastFilledQty;
	}
	public void setLastFilledQty(BigDecimal lastFilledQty) {
		this.lastFilledQty = lastFilledQty;
	}
	public BigDecimal getLastFilledPrice() {
		return lastFilledPrice;
	}
	public void setLastFilledPrice(BigDecimal lastFilledPrice) {
		this.lastFilledPrice = lastFilledPrice;
	}
	@Override
	public String toString() {
		return "OrderExecutionReport [memberId=" + memberId + ", currency=" + currency + ", symbol=" + symbol
				+ ", originalQtyBeforeScale=" + originalQtyBeforeScale + ", qty=" + qty + ", price=" + price
				+ ", executedQty=" + executedQty + ", executedPrice=" + executedPrice + ", cancelledQty=" + cancelledQty
				+ ", leavesQty=" + leavesQty + ", side=" + side + ", type=" + type + ", marketExchange="
				+ marketExchange + ", transactTime=" + transactTime + ", childOrderId=" + childOrderId + ", orderId="
				+ orderId + ", exchangeOrderId=" + exchangeOrderId + ", orderStatus=" + orderStatus
				+ ", exchangeAccountId=" + exchangeAccountId + ", childOrder=" + childOrder + ", pricePrecision="
				+ pricePrecision + ", qtyPrecision=" + qtyPrecision + ", reportType=" + reportType + ", rejectReason="
				+ rejectReason + ", rejectCode=" + rejectCode + ", lastFilledQty=" + lastFilledQty
				+ ", lastFilledPrice=" + lastFilledPrice + "]";
	}
	
}
