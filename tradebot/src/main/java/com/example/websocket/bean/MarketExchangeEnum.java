package com.example.websocket.bean;

public enum MarketExchangeEnum {
	BINANCE,
	HUOBI,
	COINBASEPRO,
	BITREX,
	E55,
	LUNO,
	GEMINI,
	SOR,
	BITTREX
}
